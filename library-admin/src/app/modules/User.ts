export class User {
    Id:string;
    Name: string;
    LastName: string;
    SubscriptionStart: Date;
    SubscriptionEnds: Date;
    RentedBooks: Array<string>; //array of ISBN13 
    Type:number;
    Password:string;
    Username:string;

    constructor(obj: any) {
        Object.assign(this, obj);
    }
}