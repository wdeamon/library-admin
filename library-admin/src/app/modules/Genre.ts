
export class Genre{
	id: string;
	name: string;

	constructor(obj:any){
		Object.assign(this, obj);
	}
}
