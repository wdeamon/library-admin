export class RentedBook {
    Id:string; 
    BookId: string;
	RentingDate: Date;
	UserId: string;

	constructor(obj: any) {
		Object.assign(this, obj);
	}
}