import { Genre } from './Genre';

export class Book {
	Format: string;
	Title:string;
	PageNumber: number;
	Dimensions: string;
	PublicationDate: Date;
	Publisher: string;
	PublicationCity: string;
	PublicationCountry: string;
	Language: string;
	ISBN10: string;
	ISBN13: string;
	ImageSrc: string;
	ShortDescription: string;
	Count: number;
	RentedTo: Array<string>;
	Author: string;
	genres: Array<string>

	constructor(obj: any) {
		Object.assign(this, obj)
	}
}

