import { BookCardModule } from './book-card.module';

describe('BookCardModule', () => {
  let bookCardModule: BookCardModule;

  beforeEach(() => {
    bookCardModule = new BookCardModule();
  });

  it('should create an instance', () => {
    expect(bookCardModule).toBeTruthy();
  });
});
