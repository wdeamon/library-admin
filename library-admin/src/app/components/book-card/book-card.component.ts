import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Book } from '../../modules/Book';
import { RentedService } from '../../service/rented.service';
import { UserService } from '../../service/user.service';
import { User } from '../../modules/User';

@Component({
  selector: 'book-card',
  templateUrl: './book-card.component.html',
  styleUrls: ['./book-card.component.scss']
})
export class BookCardComponent implements OnInit, OnDestroy {
  @Input() book: Book;
  private rentedBooks = [];
  private subs = [];
  private rentedFlag = false;
  private currentUser: User;

  constructor(
    private rentedService: RentedService,
    private userService: UserService

  ) { }

  ngOnInit() {
    this.subs.push(
      this.rentedService.rentedForUser.subscribe(data => {
        this.rentedBooks = data;
        this.rentedFlag = this.isRented(this.book);
      })
    )

    this.subs.push(
      this.userService.currentUser.subscribe(data => {
        this.currentUser = data;
      })
    );
  }

  ngOnDestroy() {
    this.subs.forEach(item => {
      item.unsubscribe();
    });
  }

  public rent() {
    if (!this.currentUser 
      || this.currentUser.Type !== 1 
      || this.book.Count - this.book.RentedTo.length <= 0 
      || this.rentedFlag
    ) {
      return;
    }

    let currentValue = this.rentedService.rentedForUser.getValue();
    currentValue.push(this.book);
    this.rentedService.rentedForUser.next(currentValue);
  }

  public isRented(book: Book): boolean {
    const rented = this.rentedBooks.find(item => {
      if (item.ISBN13 === book.ISBN13) { return true }
    });

    if (rented) {
      return true;
    }
    return false;
  }
}
