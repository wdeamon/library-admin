import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { MatDialogRef } from '@angular/material';



@Component({
  selector: 'app-login-dialog',
  templateUrl: './login-dialog.component.html',
  styleUrls: ['./login-dialog.component.scss']
})
export class LoginDialogComponent implements OnInit {

  public userData = {
    username: "",
    password: ""
  }

  constructor(
    private userService: UserService,
    private dialogRef: MatDialogRef<LoginDialogComponent>
  ) { }

  ngOnInit() { }

  public onLogin(event): void {
    this.userService.login(this.userData.username, this.userData.password);
    this.dialogRef.close();
  }
}
