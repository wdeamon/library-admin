import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ApiService } from './service/api.service';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { BookService } from './service/book.service';
import { MatDialogModule } from '@angular/material';
import { LoginDialogComponent } from './dialogs/login-dialog/login-dialog.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    LoginDialogComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    AppRoutingModule,
    MatDialogModule,
    FormsModule
  ],
  providers: [
    ApiService,
    BookService
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents:[
    LoginDialogComponent
  ]
})
export class AppModule { }
