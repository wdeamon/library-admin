import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {UserService} from '../service/user.service';
import {Router} from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class RentedGuard implements CanActivate {
  
  constructor(private userService:UserService, private router:Router){

  }
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean { 
      if(this.userService.currentUser.getValue() !== undefined){
        return true;
      }else{
        this.router.navigate(["/main/books"]);
        return false;
      }
  }
}

