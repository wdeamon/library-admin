import { TestBed, async, inject } from '@angular/core/testing';

import { RentedGuard } from './rented.guard';

describe('RentedGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RentedGuard]
    });
  });

  it('should ...', inject([RentedGuard], (guard: RentedGuard) => {
    expect(guard).toBeTruthy();
  }));
});
