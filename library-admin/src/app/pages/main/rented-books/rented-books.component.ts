import { Component, OnInit, OnDestroy } from '@angular/core';
import { BookService } from '../../../service/book.service';
import { RentedService } from '../../../service/rented.service';
import { UserService } from '../../../service/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-rented-books',
  templateUrl: './rented-books.component.html',
  styleUrls: ['./rented-books.component.scss']
})
export class RentedBooksComponent implements OnInit, OnDestroy {
  private subs = [];
  public displayBooks = [];

  constructor(
    private userService: UserService,
    private rentedService: RentedService,
    private bookService: BookService,
    private router: Router
  ) {
    this.subs.push(
      this.userService.currentUser.subscribe(data => {
        if (!data) {
          this.router.navigate(["/main/books"]);
        }
      })
    )
    this.subs.push(
      this.rentedService.rentedForUser.subscribe(
        data => {
          this.displayBooks = data;
        }
      )
    );
  }

  ngOnInit() {

  }

  ngOnDestroy() {
    this.subs.forEach(item => {
      item.unsubscribe();
    })
  }


}
