import { Component, OnInit, OnDestroy } from '@angular/core';
import { BookService } from '../../service/book.service';
import { Book } from '../../modules/Book';
import { MatDialog } from '@angular/material';
import { LoginDialogComponent } from '../../dialogs/login-dialog/login-dialog.component';
import { UserService } from '../../service/user.service';
import { User } from '../../modules/User';
import { RentedService } from '../../service/rented.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {
  private subs = [];
  public currentUser: User;
  private books = [];
  private rentedBooks = [];
  private rentedBooksForCurrentUser = [];

  constructor(
    private bookService: BookService,
    private dialog: MatDialog,
    private userService: UserService,
    private rentedService: RentedService
  ) {


    this.subs.push(
      this.bookService.store.subscribe(data => {
        this.books = data;
      })
    )

    this.subs.push(
      this.userService.currentUser.subscribe(data => {
        this.currentUser = data;
        this.filterRentedBooks();
      })
    )

    this.subs.push(
      this.rentedService.store.subscribe(
        data => {
          this.rentedBooks = data;
        }
      )
    )


    this.subs.push(
      this.userService.currentUser.subscribe(data => {
        this.currentUser = data;
      })
    );
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.subs.forEach(item => {
      item.unsubscribe();
    })
  }

  public onLogin(event): void {
    this.dialog.open(LoginDialogComponent);
  }

  public onLogout(event): void {
    this.userService.logout();
  }

  private filterRentedBooks(): void {
    if (!this.currentUser || this.currentUser.Type === 0) {
      this.rentedService.rentedForUser.next([]);
      this.rentedBooksForCurrentUser = [];
      return;
    }

    let rentedForUser = this.rentedBooks.filter(item => {
      if (item.UserId === this.currentUser.Id) { return true };
    })
    rentedForUser.forEach(item => {
      let foundBook = this.books.find(itemx => {
        if (itemx.ISBN13 === item.BookId) { return true };
      })
      if (foundBook) {
        this.rentedBooksForCurrentUser.push(foundBook)
      }
    });
    this.rentedService.rentedForUser.next(this.rentedBooksForCurrentUser);
  }
}
