import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { BooksComponent } from './books/books.component';
import { RentedBooksComponent } from './rented-books/rented-books.component';
import {BookCardModule} from '../../components/book-card/book-card.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    MainRoutingModule,
    BookCardModule,
    RouterModule
  ],
  declarations: [MainComponent, BooksComponent, RentedBooksComponent],
  exports: [MainComponent]
})
export class MainModule { }
