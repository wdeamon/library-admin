import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './main.component';
import { BooksComponent } from './books/books.component';
import { RentedBooksComponent } from './rented-books/rented-books.component';
import { RentedResolver } from '../../resolvers/rented.resolver';
import { RentedGuard } from '../../guards/rented.guard';

const appRoutes: Routes = [
  {
    path: '',
    component: MainComponent,
    resolve: {
      rented: RentedResolver,
    }, 
    children: [
      {
        path: 'books',
        component: BooksComponent,
      },
      {
        path: 'rented',
        component: RentedBooksComponent,
        canActivate: [RentedGuard]
      },
      { path: '', redirectTo: 'books', pathMatch: 'full' },
      { path: '**', redirectTo: '/404' }
    ]
  }]

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  providers: [
    RentedResolver
  ],
  exports: [
    RouterModule
  ]
})
export class MainRoutingModule { }
