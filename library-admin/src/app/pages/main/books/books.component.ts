import { Component, OnInit } from '@angular/core';
import {BookService} from '../../../service/book.service';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {

  public books = [];

  constructor(private bookService: BookService) {
    this.bookService.store.subscribe(data => {
      this.books = data;
    })
  }

  ngOnInit() {
  }

}
