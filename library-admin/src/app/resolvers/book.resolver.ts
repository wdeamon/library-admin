import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';

import { BookService} from '../service/book.service';
import {Book} from '../modules/Book';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BooksResolver implements Resolve<Array<Book>> {
  constructor(
    private router: Router,
    private bookService:BookService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Array<Book>> {
    return this.bookService.get();
  }
}