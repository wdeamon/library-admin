import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from '../service/user.service';
import { User } from '../modules/User';

@Injectable({
  providedIn: 'root'
})
export class UserResolver implements Resolve<Array<User>> {

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Array<User>> {
    return this.userService.get();
  }

}
