import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { RentedService } from '../service/rented.service';
import { RentedBook } from '../modules/RentedBook';

@Injectable({
  providedIn: 'root'
})
export class RentedResolver implements Resolve<Array<RentedBook>> {

  constructor(
    private router: Router,
    private rentedService:RentedService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Array<RentedBook>> {
    return this.rentedService.get();
  }

}
