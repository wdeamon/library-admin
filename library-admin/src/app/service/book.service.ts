import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BehaviorSubject } from 'rxjs';
import { Book } from '../modules/Book';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class BookService {
  public store: BehaviorSubject<Array<Book>> = new BehaviorSubject([]);

  constructor(private apiService: ApiService) { }

  public get(): Observable<Array<Book>> {
    return this.apiService.load("/assets/mock data/books.json").pipe(map(data => {
      let temp = [];
      data.forEach(element => {
        temp.push(new Book(element));
      });
      this.store.next(temp);
      console.log("book service");
      return temp;
    }));
  }

  public delete(obj: Book): void {

  }

  public update(obj: Book): void {

  }

  public create(obj: Book): void {

  }

}
