import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http) { }

  load(call: string): Observable<any> {
    return this.http.get(call).pipe(map(response => {
      return JSON.parse((<any>response)._body);
    }))
  }
}
