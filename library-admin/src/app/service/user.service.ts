import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { User } from '../modules/User';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public store: BehaviorSubject<Array<User>> = new BehaviorSubject([]);
  public currentUser: BehaviorSubject<User | undefined> = new BehaviorSubject(undefined);

  constructor(private apiService: ApiService) { }

  public get() {
    return this.apiService.load("/assets/mock data/users.json").pipe(map(response => {
      let temp = [];
      response.forEach(element => {
        temp.push(new User(element));
      });
      this.store.next(temp);
      console.log("user service");
      return temp;
    }))
  }

  public login(username: string, password: string) {
    let user = this.store.getValue().find(item => {
      if (item.Username === username && item.Password === password) { return true }
    });
    this.currentUser.next(user);
  }

  public logout() {
    this.currentUser.next(undefined);
  }
}
