import { Injectable } from '@angular/core';
import { Genre } from '../modules/Genre';
import { ApiService } from './api.service';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class GenreService {
  public store: BehaviorSubject<Array<Genre>> = new BehaviorSubject([]);

  constructor(private apiService: ApiService) { }

  public get(): Observable<Array<Genre>> {
    return this.apiService.load("/assets/mock data/genres.json").pipe(map(response => {
      let temp = [];
      response.forEach(element => {
        temp.push(new Genre(element));
      });
      return temp;
    }))
  }

  public delete() {

  }

  public update() {


  }

  public create() {

  }
}
