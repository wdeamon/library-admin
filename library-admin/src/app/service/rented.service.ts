import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs'
import { RentedBook } from '../modules/RentedBook';
import { Book } from '../modules/Book';

@Injectable({
  providedIn: 'root'
})
export class RentedService {
  public store: BehaviorSubject<Array<RentedBook>> = new BehaviorSubject([]);
  public rentedForUser: BehaviorSubject<Array<Book>> = new BehaviorSubject([]);

  constructor(private apiService: ApiService) { }

  public get(): Observable<Array<RentedBook>> {
    return this.apiService.load("/assets/mock data/rentedBooks.json").pipe(map(data => {
      let temp = [];
      data.forEach(element => {
        temp.push(new RentedBook(element));
      });
      this.store.next(temp);
      return temp;
    }));
  }

  public delete(obj: RentedBook): void {

  }

  public update(obj: RentedBook): void {

  }

  public create(obj: RentedBook): void {

  }

}

