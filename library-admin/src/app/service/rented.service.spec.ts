import { TestBed, inject } from '@angular/core/testing';

import { RentedService } from './rented.service';

describe('RentedService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RentedService]
    });
  });

  it('should be created', inject([RentedService], (service: RentedService) => {
    expect(service).toBeTruthy();
  }));
});
