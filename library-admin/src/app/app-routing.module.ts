import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { BooksResolver } from './resolvers/book.resolver';
import { UserResolver } from './resolvers/user.resolver';
import { MainComponent } from './pages/main/main.component';

const appRoutes: Routes = [
  {
    path: 'main',
    //component: MainComponent,
    loadChildren: "./pages/main/main.module#MainModule",
    resolve: {
      books: BooksResolver,
      users: UserResolver
    }
  },
  { path: '', redirectTo: "/main", pathMatch: 'full' },
  { path: '404', redirectTo: "/" },
  { path: '**', redirectTo: '/404' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    BooksResolver,
    UserResolver
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
